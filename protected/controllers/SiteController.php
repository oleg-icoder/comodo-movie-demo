<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		if (Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->user->loginUrl);
		}

		$currentPage = Yii::app()->request->getQuery('page', 1);
		$category = Yii::app()->request->getQuery('category', 'popular');
		$categoryMap = array(
			'popular' => 'getPopularMovies',
			'fresh' => 'getFreshMovies'
		);
		$fetchMethod = array_key_exists($category, $categoryMap) ? $categoryMap[$category] : current($categoryMap);
		$movies = Yii::app()->user->tmdbApi->$fetchMethod($currentPage);
		if ($movies === false) {
			return $this->render('index', array('error' => Yii::app()->user->tmdbApi->getError()));
		}

		// results per page
		$pages = new CPagination($movies['total_results']);
		$pages->setPageSize(sizeof($movies['results']));
		$pages->setCurrentPage($currentPage - 1);
		$error = null;

		$this->render('index', compact('movies', 'pages', 'error'));
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionMovie()
	{
		if (Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->user->loginUrl);
		}
		$movieId = Yii::app()->request->getQuery('id', 0);
		$editMovie = Yii::app()->request->getPost('Movie', false);
		$movie = Movie::model()->findByPk($movieId);
		if ($editMovie !== false) {
			$this->saveMovieDetails($editMovie, $movie);
		} elseif ($movie === null) {
			$movie = $this->getMovieDetails($movieId);
			if ($movie === null) {
				return $this->render('movie', array('error' => Yii::app()->user->tmdbApi->getError()));
			}
			$movie = $this->saveMovieDetails($movie);
		}
		$error = null;
		$this->render('movie', compact('movie', 'error'));
	}

	private function getMovieDetails($movieId) {
		$movie = Yii::app()->user->tmdbApi->getMovieDetails($movieId);
		if ($movie === false) return null;

		$movie['poster_path'] = Yii::app()->user->tmdbApi->getImageUrl() . $movie['poster_path'];
		$movie['genres'] = array_reduce($movie['genres'], function ($result, $item) {
			if (!is_array($result)) $result = array();
			$result[] = $item['name'];
			return $result;
		});
		$movie['genres'] = implode(', ', $movie['genres']);
		return $movie;
	}
	private function saveMoviePoster($movieId, $posterUrl) {
		$savePath = '/images/'.$movieId.'.jpg';
		$result = file_put_contents(Yii::getPathOfAlias('webroot').$savePath, fopen($posterUrl, 'r'));
		return $result === false ? $result : Yii::app()->getBaseUrl(true).$savePath;
	}
	private function saveMovieDetails($data, $movie = null) {
		$fieldList = array(
			'id',
			'title',
			'original_title',
			'release_date',
			'runtime',
			'overview',
			'genres',
		);

		if ($movie === null) {
			$movie = new Movie;
			$fieldList[] = 'poster_path';
			$data['poster_path'] = $this->saveMoviePoster($data['id'], $data['poster_path']);
		}

		foreach ($fieldList as $field) {
			$movie->$field = $data[$field];
		}
		$movie->save();
		return $movie;
	}

	/**
	 * This is the action to delete movie info.
	 */
	public function actionDeleteMovie() {
		if (Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->user->loginUrl);
		}
		$movieId = Yii::app()->request->getQuery('id', 0);
		$movie = Movie::model()->findByPk($movieId);
		if ($movie !== null) $movie->delete();
		$this->redirect(array('site/movie', 'id' => $movieId));
	}

	/**
	 * This is the action to rate movie.
	 */
	public function actionRateMovie() {
		if (Yii::app()->user->isGuest) {
			echo 'You need to be logged in';
			Yii::app()->end();
		}
		$movieId = Yii::app()->request->getQuery('id', 0);
		$rating = Yii::app()->request->getQuery('rating', 0);
		Yii::app()->user->tmdbApi->rateMovie($movieId, $rating);
		$error = Yii::app()->user->tmdbApi->getError();
		echo is_null($error) ? 'Your rating has been successfully saved.' : $error;
		Yii::app()->end();
	}


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}