<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
$category = Yii::app()->request->getQuery('category', 'popular');
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>
<?php echo CHtml::link('Popular',array(
	'site/index',
	'category' => 'popular'
), array(
	'class' => $category === 'popular' ? 'selected' : '',
)); ?>
&nbsp;
<?php echo CHtml::link('Now in Cinemas', array(
	'site/index',
	'category' => 'fresh'
), array(
	'class' => $category === 'fresh' ? 'selected' : ''
)); ?>
</p>

<?php if ($error === null) : ?>
	<table>
		<tr><th>
			Id
		</th><th>
			Title
		</th><th>
			Release Date
		</th></tr>
		<?php foreach ($movies['results'] as $movie) : ?>
			<tr><td>
				<?php echo CHtml::link($movie['id'], array(
					'site/movie',
					'id' => $movie['id']
				)); ?>
			</td><td>
				<?=$movie['title']?>
			</td><td>
				<?=$movie['release_date']?>
			</td></tr>
		<?php endforeach ?>
	</table>

	<p>
	<?php $this->widget('CLinkPager', array(
		'pages' => $pages,
		'maxButtonCount' => 4
	)) ?>
	</p>
<?php else : ?>
	<p><strong>Error:</strong> <?=$error?></p>
<?php endif ?>