<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">For example: 6967a478739397ccbfbbb5951e2a2fa2</p>

	<div class="row">
		<?php echo $form->labelEx($model,'tmdbApiKey', array('label' => 'API Key')); ?>
		<?php echo $form->textField($model,'tmdbApiKey'); ?>
		<?php echo $form->error($model,'tmdbApiKey'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Login'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
