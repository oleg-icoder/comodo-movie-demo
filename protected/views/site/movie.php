<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>

<h1>Movie details</h1>

<?php if ($error === null) : ?>
<ul>
	<li><img src="<?=$movie['poster_path']?>" /></li>
	<li><em>Title:</em> <?=$movie['title']?></li>
	<li><em>Original Title:</em> <?=$movie['original_title']?></li>
	<li><em>Release Date:</em> <?=$movie['release_date']?></li>
	<li><em>Runtime:</em> <?=$movie['runtime']?></li>
	<li><em>Overview:</em> <?=$movie['overview']?></li>
	<li><em>Genres:</em> <?=$movie['genres']?></li>
</ul>

<?php echo CHtml::link('Delete Movie Information',"#", array(
		'submit' => array('deleteMovie', 'id' => $movie['id']),
		'confirm' => 'Are you sure want to delete information about this movie from DB?')
); ?>

<br />	<br />

<h3>Rate Movie:</h3>
<?php for ($i = 1; $i <= 10; $i++) : ?>
	<?php echo CHtml::ajaxLink('*', array('site/rateMovie', 'id' => $movie['id'], 'rating' => $i),
		array(
			'update'=>'#ratingResult',
		),
		array(
			//'submit' => array('rateMovie', 'id' => $movie['id'], 'rating' => $i),
			'confirm' => "Want to rate this movie {$i} stars?",
			'class' => 'rating-star'
		)); ?>
<?php endfor ?>
<div id="ratingResult"></div>

<br /><br />

<div class="form">
<h3>Edit Movie Information:</h3>
<?php $form=$this->beginWidget('CActiveForm'); ?>

<?php echo $form->errorSummary($movie); ?>

<div class="row">
	<?php echo $form->label($movie,'title'); ?>
	<?php echo $form->textField($movie,'title') ?>
</div>

<div class="row">
	<?php echo $form->label($movie,'original_title'); ?>
	<?php echo $form->textField($movie,'original_title') ?>
</div>

<div class="row">
	<?php echo $form->label($movie,'release_date'); ?>
	<?php echo $form->dateField($movie,'release_date') ?>
</div>

<div class="row">
	<?php echo $form->label($movie,'runtime'); ?>
	<?php echo $form->numberField($movie,'runtime') ?>
</div>

<div class="row">
	<?php echo $form->label($movie,'overview'); ?>
	<?php echo $form->textArea($movie,'overview', array('cols' => 100, 'rows' => 8)) ?>
</div>

<div class="row">
	<?php echo $form->label($movie,'genres'); ?>
	<?php echo $form->textField($movie,'genres', array('size' => 98)) ?>
</div>

<?php echo $form->hiddenField($movie,'id') ?>
<div class="row submit">
	<?php echo CHtml::submitButton('Edit'); ?>
</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<?php else : ?>
	<p><strong>Error:</strong> <?=$error?></p>
<?php endif ?>