<?php

class m160809_121513_crate_movie_db_structure extends CDbMigration
{
	public function up()
	{
		$this->createTable('movie', array(
			'id'				=> 'pk',
			'title'				=> 'string',
			'original_title'	=> 'string',
			'release_date'		=> 'date',
			'runtime'			=> 'integer',
			'overview'			=> 'text',
			'genres'			=> 'text',
			'poster_path'		=> 'string',
		));
	}

	public function down()
	{
		$this->dropTable('movie');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}