<?php

/**
 * TmdbApi class send basic requests to TMDB API and fetch data.
 */

class TmdbApi extends CApplicationComponent {
	private $apiKey = null;
	private $serviceUrl = 'http://private-anon-33e4818bd0-themoviedb.apiary-proxy.com/3';
	private $errorMessage = null;
	private $guestSessionId = null;
	private $expirationTimestamp = 0;
	private $imageUrl = 'http://image.tmdb.org/t/p/w185/';
	public function __construct($apiKey){
		$this->setApiKey($apiKey);
	}

	private function setExpirationDate($stringDate) {
		$this->expirationTimestamp = strtotime($stringDate);
	}
	public function getExpirationDate() {
		return $this->expirationTimestamp;
	}
	public function startGuestSession(){
		$response =  $this->sendRequest('/authentication/guest_session/new');
		if ($response === false or !isset($response['guest_session_id'])) return false;
		$this->setExpirationDate($response['expires_at']);
		$this->guestSessionId = $response['guest_session_id'];

		return $response['guest_session_id'];
	}

	public function getPopularMovies($page = 1){
		$response =  $this->sendRequest('/discover/movie', array(
			'page' => $page,
			'sort_by' => 'popularity.desc')
		);
		return $response;
	}
	public function getFreshMovies($page = 1){
		$response =  $this->sendRequest('/discover/movie', array(
			'page' => $page,
			'sort_by' => 'release_date.desc',
			'primary_release_date.lte' => date('Y-m-d'),
			'primary_release_date.gte' => date('Y-m-d', strtotime('-2 month'))
		));
		return $response;
	}
	public function getMovieDetails($id){
		return $this->sendRequest("/movie/{$id}");
	}

	public function rateMovie($id, $rating) {
		return  $this->sendRequest("/movie/{$id}/rating", array(
			'guest_session_id' => $this->guestSessionId
		), array(
			'value' => $rating
		));
	}
	private function sendRequest($path, $params = array(), $postData = null){
		if (!is_array($params)) return $this->setError('Request parameters should be passed in array');

		$params = array_merge(array('api_key' => $this->apiKey), $params);
		$path = $this->serviceUrl . $path;
		if ($postData === null) {
			$response = Yii::app()->curl->get($path, $params);
		} else {
			$response = Yii::app()->curl->post($path, $postData, $params);
		}
		if (Yii::app()->curl->getError() !== null) {
			return $this->setError(Yii::app()->curl->getError() . Yii::app()->curl->getInfo());
		}
		$response = json_decode($response, true);
		if (isset($response['status_code']) and $response['status_code'] != 1) {
			return $this->setError($response['status_message']);
		}

		return $response;
	}

	public function setApiKey($apiKey){
		if (!empty($apiKey)) $this->apiKey = $apiKey;
	}

	private function setError($message){
		$this->errorMessage = $message;
		return false;
	}

	public function getError(){
		return $this->errorMessage;
	}

	public function getImageUrl(){
		return $this->imageUrl;
	}
}