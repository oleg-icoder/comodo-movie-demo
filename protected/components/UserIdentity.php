<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CBaseUserIdentity
{
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public $tmdbApi;
	public function __construct($apiKey) {
		$this->tmdbApi = new TmdbApi($apiKey);;
	}
	public function authenticate()
	{
		$guestSessionId = $this->tmdbApi->startGuestSession();
		if ($guestSessionId === false) {
			$this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
			$this->errorMessage = $this->tmdbApi->getError();
		} else {
			$this->errorCode = self::ERROR_NONE;
			$this->setState('tmdbApi', $this->tmdbApi);
		}
		return !$this->errorCode;
	}
}