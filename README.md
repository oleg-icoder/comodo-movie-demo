# README #

Demo site on Yii using TMDB API.

### What is this repository for? ###

* Quick summary: Just to show my skills with Yii
* Version: 1

### How do I get set up? ###

* Go to "protected" directory and run "php composer.phar install"
* Type yes to apply DB migration (create 'movies' table)